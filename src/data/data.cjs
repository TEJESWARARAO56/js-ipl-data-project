const csv = require('csvtojson');
const fs = require('fs');

const bestEconomyBowlerInSuperOver = require('../server/9-best-economy-bowler-in-super-over.cjs');

const getPlayerWhoDismissedMostOfTimesByAnotherPlayer = require('../server/8-player-dismissed-most-of-times-by-bowler.cjs');

const getStrikeRateOfBatsmanForEachSeason = require('../server/7-strike-rate-of-batsman-in-each-season.cjs');

const getTopTnEconomicalBowlerIn2015 = require('../server/4-top-ten-economical-bowlers-in-2015.cjs');

const getMostNumberOfThePlayerOfTheMatchesInEachSeason = require('../server/6-who-won-highest-number-of-player-of-the-match-in-each-season.cjs')

const getNumberOfTimesEachTeamWonTheTossAndAlsoMatch = require('../server/5-number-times-each-team-won-toss-and-matches.cjs')

const getExtraRunsConceededByEachTeamIn2016 = require('../server/3-extra-runs-conceeded-per-team-in-2016.cjs')

const getMatchesWonPerTeamPerYear = require('../server/2-matches-won-per-team-year');

const getMatchesPerYear = require('../server/1-matches-per-year.cjs');

let deliveriesData;
let matchesData;

async function convertCsvToJson(matchescsv, deliveriescsv) {
  try {
    matchesData = await csv().fromFile(matchescsv);
    deliveriesData = await csv().fromFile(deliveriescsv);

    let bestEconomyBowlerInSuperOvers = bestEconomyBowlerInSuperOver(deliveriesData)
    await fs.promises.writeFile('../public/output/bestEconomyBowlerInSuperOver.json',
      JSON.stringify(bestEconomyBowlerInSuperOvers), 'utf-8');
    let playerDismissedMostOfTimesByAnotherPlayer = getPlayerWhoDismissedMostOfTimesByAnotherPlayer(deliveriesData);
    await fs.promises.writeFile('../public/output/playerDismissedMostOfTimesByBowler.json',
      JSON.stringify(playerDismissedMostOfTimesByAnotherPlayer), 'utf-8');
    let strikeRatesofBatsmansInEachSeason = getStrikeRateOfBatsmanForEachSeason(deliveriesData, matchesData)
    await fs.promises.writeFile('../public/output/strikeRateOfBatsmanInEachSeason.json',
      JSON.stringify(strikeRatesofBatsmansInEachSeason), 'utf-8');
    let topTnEconomicalBowlerIn2015 = getTopTnEconomicalBowlerIn2015(deliveriesData, matchesData)
    await fs.promises.writeFile('../public/output/topTenEconomicalBowlersIn2015.json',
      JSON.stringify(topTnEconomicalBowlerIn2015), 'utf-8');
    let mostNumberOfManOfTheMatchesInEachYear = getMostNumberOfThePlayerOfTheMatchesInEachSeason(matchesData);
    await fs.promises.writeFile('../public/output/playerWhoWonHighestNumberOfPlayerOfTheMatchInEachSeason.json',
      JSON.stringify(mostNumberOfManOfTheMatchesInEachYear), 'utf-8');
    let numberOfTimesTossWonAndMatchWon = getNumberOfTimesEachTeamWonTheTossAndAlsoMatch(matchesData)
    await fs.promises.writeFile('../public/output/numberTimesEachTeamWonTossAndMatches.json',
      JSON.stringify(numberOfTimesTossWonAndMatchWon), 'utf-8');
    let extraRunsConceededByEachTeamIn2016 = getExtraRunsConceededByEachTeamIn2016(deliveriesData, matchesData);
    await fs.promises.writeFile('../public/output/extraRunsConceededPerTeamIn2016.json',
      JSON.stringify(extraRunsConceededByEachTeamIn2016), 'utf-8');
    let matchesWonPerTeamPerYear = getMatchesWonPerTeamPerYear(matchesData);
    await fs.promises.writeFile('../public/output/matchesWonPerTeamPerYear.json',
      JSON.stringify(matchesWonPerTeamPerYear), 'utf-8');
    let matchesPerYear = getMatchesPerYear(matchesData);
    await fs.promises.writeFile('../public/output/matchesPerYear.json',
      JSON.stringify(matchesPerYear), 'utf-8');
  } catch (error) {
    console.error(`Error converting file: ${error}`);
  }
}

convertCsvToJson('matches.csv', 'deliveries.csv');
