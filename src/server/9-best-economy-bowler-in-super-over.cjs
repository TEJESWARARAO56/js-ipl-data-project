function bestEconomyBowlerInSuperOver(deliveries) {
    let bowlerStatsObjectIndex = 1
    let topBowlerIndex = 0
    let numberOfDecimals = 2
    let bowlerNameIndex = 0
    // this reduce method return object of bowler and ballas and runs conceeded
    let bestEconomyBowlerInSuperOver = deliveries
        .reduce((accumulator, delivery) => {
            if (delivery.is_super_over === "1") {
                let ballRuns = parseInt(delivery.total_runs) -
                    parseInt(delivery.legbye_runs) - parseInt(delivery.bye_runs)
                accumulator[delivery['bowler']] = accumulator[delivery['bowler']] || { balls: 0, runs: 0 }
                if (delivery.noball_runs === "0" && delivery.wide_runs === "0") {
                    accumulator[delivery['bowler']]['balls'] += 1
                }
                accumulator[delivery['bowler']]['runs'] += ballRuns
            }
            return accumulator
        }, {})
    // getting array of arrays and finding economy rate of each bowler using map function
    let bowlerBallsRunsInList = Object.entries(bestEconomyBowlerInSuperOver)
    let bowlerEconomyInList = bowlerBallsRunsInList.map((bowlerStats) => {
        return [bowlerStats[bowlerNameIndex],
        Number((bowlerStats[bowlerStatsObjectIndex]['runs'] * 6 /
            bowlerStats[bowlerStatsObjectIndex]['balls'])
            .toFixed(numberOfDecimals))
        ]
    })
    // sorting the array elements and accessing top bowler
    let orderedBowlersEconomy = (bowlerEconomyInList.sort((bowler1, bowler2) =>
        bowler1[bowlerStatsObjectIndex] - bowler2[bowlerStatsObjectIndex]))
    return Object.fromEntries(orderedBowlersEconomy.slice(0, 11))

}
module.exports = bestEconomyBowlerInSuperOver