function getTopTnEconomicalBowlerIn2015(deliveries, matches) {
    let bowlerNameIndex = 0
    let bowlerStatsObjectIndex = 1
    let zerothIndex = 0;
    let eleventhIndex = 11
    let numberOfDecimals = 2
    let matchIdWithSeason = matches.reduce((accumulator, match) => {
        accumulator[match.id] = match.season
        return accumulator
    }, {})
    // creating object that holds balls and runs conceeded by each bowler
    let bowlerBallsAndRunsConceeded = deliveries
        .reduce((accumulator, delivery) => {
            if (matchIdWithSeason[delivery.match_id] === "2015") {
                accumulator[delivery['bowler']] = accumulator[delivery['bowler']] || { balls: 0, runs: 0 }
                if (delivery.noball_runs === "0" || delivery.wide_runs === "0") {
                    accumulator[delivery['bowler']]['balls'] += 1
                }
                accumulator[delivery['bowler']]['runs'] += parseInt(delivery.total_runs) -
                    parseInt(delivery.legbye_runs) - parseInt(delivery.bye_runs)
            }
            return accumulator
        }, {})
    // calculating economy of each bowler using map function
    let bowlerBallsRunsInList = Object.entries(bowlerBallsAndRunsConceeded)
    let bowlerEconomyInList = bowlerBallsRunsInList.map((bowlerStats) => {
        return [bowlerStats[bowlerNameIndex], {
            ...bowlerStats[bowlerStatsObjectIndex], economy:
                Number((bowlerStats[bowlerStatsObjectIndex]['runs'] * 6 /
                    bowlerStats[bowlerStatsObjectIndex]['balls']).toFixed(numberOfDecimals))
        }]
    })
    // sorting bowlers according to the bowler economy and getting top ten bowlers
    let orderedBowlersEconomy = (bowlerEconomyInList.sort((player1, player2) =>
        player1[bowlerStatsObjectIndex].economy - player2[bowlerStatsObjectIndex].economy))
    let topTenBowlerEconomy = Object.fromEntries(orderedBowlersEconomy.slice(zerothIndex, eleventhIndex))
    return topTenBowlerEconomy
}
module.exports = getTopTnEconomicalBowlerIn2015