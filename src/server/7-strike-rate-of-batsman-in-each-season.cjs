function getStrikeRateOfBatsmanForEachSeason(deliveries, matches) {
  let numberOfDecimals = 2;
  let seasonIndex = 0;
  let seasonBallsRunsIndex = 1;
  // creating object that holds match id and season
  let matchIdWithSeason = matches.reduce((accumulator, match) => {
    accumulator[match.id] = match.season;
    return accumulator;
  }, {});
  // creating object that holds batsman and nested object that holds season and balls and runs
  let ballsAndRunsOfBatsmansInEachSeason = deliveries.reduce(
    (accumulator, delivery) => {
      accumulator[matchIdWithSeason[delivery.match_id]] =
        accumulator[matchIdWithSeason[delivery.match_id]] || {};
      accumulator[matchIdWithSeason[delivery.match_id]][delivery.batsman] =
        accumulator[matchIdWithSeason[delivery.match_id]][delivery.batsman] || { balls: 0, runs: 0, };
      if (delivery.wide_runs === "0") {
        accumulator[matchIdWithSeason[delivery.match_id]][delivery["batsman"]]["balls"] += 1;
      }
      accumulator[matchIdWithSeason[delivery.match_id]][delivery["batsman"]]["runs"] +=
        parseInt(delivery["batsman_runs"]);
      return accumulator;
    }, {});
  // converting into array of arrays and calculting strikerate for each season
  let strikeRatesofBatsmansInEachSeason = Object.entries(ballsAndRunsOfBatsmansInEachSeason)
    .map((batsman) => {
      return [batsman[0], Object.fromEntries(
        Object.entries(batsman[1]).map((season) => {
          return [season[seasonIndex],

            // ...season[seasonBallsRunsIndex],
            // strike_rate: 
            Number(((season[seasonBallsRunsIndex].runs * 100) /
              season[seasonBallsRunsIndex].balls).toFixed(numberOfDecimals)),
            ,];
        }))];
    });
  // finally converting into object that holds batsman strikerate in each season
  return strikeRatesofBatsmansInEachSeason
}
module.exports = getStrikeRateOfBatsmanForEachSeason;
