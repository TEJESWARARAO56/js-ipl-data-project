function getMatchesWonPerTeamPerYear(matches) {
    // returning object that holds number of matches won by each team in each season
    return matches.reduce((accumulator, match) => {
        if (match.winner === "") {
            return accumulator
        }
        accumulator[match['season']] = accumulator[match['season']] || {}
        accumulator[match['season']][match['winner']] =
            accumulator[match['season']][match['winner']] + 1 || 1
        return accumulator
    }, {})
}
module.exports = getMatchesWonPerTeamPerYear