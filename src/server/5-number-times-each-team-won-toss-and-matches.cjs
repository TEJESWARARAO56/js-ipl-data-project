function getNumberOfTimesEachTeamWonTheTossAndMatches(matches) {

    // creating object that holds number of times each team won toss and also won match 
    let numberOfTimesTossWonAndMatchWon = matches.reduce((accumulator, match) => {
        accumulator[match['toss_winner']] = accumulator[match['toss_winner']] || { 'won_toss': 0, 'won_match': 0 }
        accumulator[match['toss_winner']]['won_toss'] += 1
        if (match['toss_winner'] === match['winner']) {
            accumulator[match['toss_winner']]['won_match'] += 1
        }
        return accumulator
    }, {})
    return numberOfTimesTossWonAndMatchWon
}

module.exports = getNumberOfTimesEachTeamWonTheTossAndMatches