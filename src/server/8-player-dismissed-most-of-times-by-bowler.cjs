function getPlayerWhoDismissedMostOfTimesByAnotherPlayer(deliveries) {
    let specialWaysOfDismissals = ['run out', 'retired hurt', 'obstructing the field', ""]
    // creating object that holds how many times bowler dismisssed batsman
    let numberTimesBowlerDismissedBatsman = deliveries.reduce((accumulator, delivery) => {
        if (specialWaysOfDismissals.includes(delivery['dismissal_kind'])) {
            return accumulator
        }
        accumulator[delivery['bowler']] = accumulator[delivery['bowler']] || {}
        accumulator[delivery['bowler']][delivery['batsman']] =
            accumulator[delivery['bowler']][delivery['batsman']] + 1 || 1
        return accumulator
    }, {})
    // sorting number of dismissals
    let listOfBowlersAndBatterDismissals = Object.entries(numberTimesBowlerDismissedBatsman)
        .reduce((accumulator, bowler) => {
            accumulator = accumulator.concat(Object.entries(bowler[1]).map((batsman) => {
                return [bowler[0], batsman]
            }))
            return accumulator
        }, []).sort((batsman1, batsman2) => {
            return batsman2[1][1] - batsman1[1][1]
        })
    return listOfBowlersAndBatterDismissals
}
module.exports = getPlayerWhoDismissedMostOfTimesByAnotherPlayer