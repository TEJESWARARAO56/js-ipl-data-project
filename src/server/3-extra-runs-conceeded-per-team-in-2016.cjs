function getExtraRunsConceededInEachTeamIn2016(deliveries, matches) {
    // creating object that holds object of match_id and season
    let matchIdWithSeason = matches.reduce((accumulator, match) => {
        accumulator[match.id] = match.season
        return accumulator
    }, {})
    // creating object that contains team and extra runs conceeded
    let extraRunsConceededByEachteamInTheYear2016 = deliveries.reduce((accumulator, delivery) => {
        if (matchIdWithSeason[delivery['match_id']] === "2016") {
            accumulator[delivery['bowling_team']] = accumulator[delivery['bowling_team']] +
                parseInt(delivery['extra_runs']) || parseInt(delivery['extra_runs'])
        }
        return accumulator
    }, {})
    return extraRunsConceededByEachteamInTheYear2016
}
module.exports = getExtraRunsConceededInEachTeamIn2016