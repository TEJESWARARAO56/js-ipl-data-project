function getMostNumberOfThePlayerOfTheMatchesInEachSeason(matches) {
    const seasonIndex = 0;
    const numberOfMOMIndex = 1
    const highestManOfTheMatchesPlayerIndex = 0
    const playerMOMIndexInseasonStats = 1

    // creting object that contains seasons and each season holds player name and how many times he won MOM
    let numberOfManOfTheMatchesByPlayerInEachYear = matches.reduce((accumulator, match) => {
        accumulator[match['season']] = accumulator[match['season']] || {}
        accumulator[match['season']][match['player_of_match']] =
            accumulator[match['season']][match['player_of_match']] + 1 || 1
        return accumulator
    }, {})
    // sorting players in in each season according number of times they won MOM
    // and access top player in each season
    let sortedListOfNumberOfManOfTheMatchesInEachSeason =
        Object.entries(numberOfManOfTheMatchesByPlayerInEachYear)
            .map((season) => {
                return [season[seasonIndex], Object.entries(season[playerMOMIndexInseasonStats])
                    .sort((player1, player2) => {
                        return player2[numberOfMOMIndex] - player1[numberOfMOMIndex]
                    })[highestManOfTheMatchesPlayerIndex]]
            })
    // converting into object and returning
    let objectOfPlayersWonMostNumberOfMatchesInEachYear =
        Object.fromEntries(sortedListOfNumberOfManOfTheMatchesInEachSeason)
    return objectOfPlayersWonMostNumberOfMatchesInEachYear
}

module.exports = getMostNumberOfThePlayerOfTheMatchesInEachSeason