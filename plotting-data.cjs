
let matchesPerYear;
let extraRunsConceeded;
let matchesWonPerTeamPerYear;
let numberOfTimesWonTossAndWonMatch;
let topTenEconomicalBowlersIn2015;
let playerWhoWonMostMOM;
let strikeOfBatsmanInEachSeason;
let numberOfTimesPlayerDismissalAnotherPlayer;
let bestEconomyInSuperOver;
async function plotData() {
    console.log(89);
    matchesPerYear = await fetch('./src/public/output/matchesPerYear.json')
    matchesPerYear = await matchesPerYear.json()
    console.log(matchesPerYear)
    extraRunsConceeded = await fetch('./src/public/output/extraRunsConceededPerTeamIn2016.json')
    extraRunsConceeded = await extraRunsConceeded.json();

    matchesWonPerTeamPerYear = await fetch('./src/public/output/matchesWonPerTeamPerYear.json')
    matchesWonPerTeamPerYear = await matchesWonPerTeamPerYear.json();
    matchesWonPerTeamPerYear = Object.entries(matchesWonPerTeamPerYear);

    numberOfTimesWonTossAndWonMatch = await fetch('./src/public/output/numberTimesEachTeamWonTossAndMatches.json')
    numberOfTimesWonTossAndWonMatch = await numberOfTimesWonTossAndWonMatch.json();
    let listOfTeamsWithTossWonAndMatchesWon = Object.entries(numberOfTimesWonTossAndWonMatch);

    topTenEconomicalBowlersIn2015 = await fetch('./src/public/output/topTenEconomicalBowlersIn2015.json');
    topTenEconomicalBowlersIn2015 = await topTenEconomicalBowlersIn2015.json();
    let listOfTopTenBowlers = Object.entries(topTenEconomicalBowlersIn2015)

    playerWhoWonMostMOM = await fetch('./src/public/output/playerWhoWonHighestNumberOfPlayerOfTheMatchInEachSeason.json');
    playerWhoWonMostMOM = await playerWhoWonMostMOM.json();
    let listOfPlayerWithMostMOM = Object.entries(playerWhoWonMostMOM);

    strikeOfBatsmanInEachSeason = await fetch('./src/public/output/strikeRateOfBatsmanInEachSeason.json');
    strikeOfBatsmanInEachSeason = await strikeOfBatsmanInEachSeason.json();

    numberOfTimesPlayerDismissalAnotherPlayer = await fetch('./src/public/output/playerDismissedMostOfTimesByBowler.json');
    numberOfTimesPlayerDismissalAnotherPlayer = await numberOfTimesPlayerDismissalAnotherPlayer.json();

    bestEconomyInSuperOver = await fetch('./src/public/output/bestEconomyBowlerInSuperOver.json');
    bestEconomyInSuperOver = await bestEconomyInSuperOver.json();

    
    Highcharts.chart('plot-one', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches Played in each year',
            align: 'left',
            x: 70,
            style: {
                color: '#100445',
                fontSize: '27px',
                fontFamily: 'Arial, sans-serif',
                fontWeight: "600"
            }
        },
        xAxis: {
            categories: Object.keys(matchesPerYear),
            crosshair: true,
            labels: {
                style: {
                    color: 'black',
                    fontSize: '13px',
                    fontFamily: 'Arial, sans-serif',
                    fontWeight: '500' 
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of matches',
                style: {
                    color: 'black', 
                    fontSize: '22px', 
                    fontFamily: 'Arial, sans-serif', 
                    fontWeight: '500' 
                }
            }
        },
        tooltip: {
            animation: 2
        },
        series: [{
            name: 'Number of matches',
            data: Object.values(matchesPerYear),
            color: 'green'
        }]
    })

    let teamNames = matchesWonPerTeamPerYear.reduce((acc, season) => {
        acc = new Set([...acc, ...Object.entries(season[1]).map((team) => team[0])])
        return acc
    }, new Set())
    teamNames = Array.from(teamNames);

    Highcharts.chart('plot-two', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Number of matches won by each team in each year',
            align: 'left',
            x: 70,
            style: {
                color: '#100445',
                fontSize: '27px',
                fontFamily: 'Arial, sans-serif',
                fontWeight: "600"
            }
        },
        xAxis: {
            categories: matchesWonPerTeamPerYear.map((season) => season[0]),
            crosshair: true,
            labels: {
                style: {
                    color: 'black',
                    fontSize: '13px',
                    fontFamily: 'Arial, sans-serif',
                    fontWeight: '500' 
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of matches',
                style: {
                    color: 'black', 
                    fontSize: '22px', 
                    fontFamily: 'Arial, sans-serif', 
                    fontWeight: '500' 
                }
            }
        },
        tooltip: {
            animation: 2,
            shared: true,
        },
    series: teamNames.map((team) => {
        return {
            name: team,
            pointPadding: 0.05,
            groupPadding: 0.1,
            data: matchesWonPerTeamPerYear.map((season) => {
                return season[1][team] || 0;
            })
        }
    })
    })

    Highcharts.chart('plot-three', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Extra runs conceeded by each team',
            align: 'left',
            x: 70,
            style: {
                color: '#100445',
                fontSize: '27px',
                fontFamily: 'Arial, sans-serif',
                fontWeight: "600"
            }
        },
        xAxis: {
            categories: Object.keys(extraRunsConceeded),
            crosshair: true,
            labels: {
                style: {
                    color: 'black',
                    fontSize: '13px',
                    fontFamily: 'Arial, sans-serif',
                    fontWeight: '500' 
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Extra runs conceeded',
                style: {
                    color: 'black', 
                    fontSize: '22px', 
                    fontFamily: 'Arial, sans-serif', 
                    fontWeight: '500' 
                }
            }
        },
        tooltip: {
            animation: 2
        },
        series: [{
            name: "Extra runs conceeded",
            data: Object.values(extraRunsConceeded),
            color: '#544443'
        }]
    })


    Highcharts.chart('plot-four', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Number of matches win toss and win matches',
            align: 'left',
            x: 70,
            style: {
                color: '#100445',
                fontSize: '27px',
                fontFamily: 'Arial, sans-serif',
                fontWeight: "600"
            }
        },

        xAxis: {
            categories: Object.keys(numberOfTimesWonTossAndWonMatch),
            crosshair: true,
            labels: {
                style: {
                    color: 'black',
                    fontSize: '13px',
                    fontFamily: 'Arial, sans-serif',
                    fontWeight: '500' 
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of matches',
                style: {
                    color: 'black', 
                    fontSize: '22px', 
                    fontFamily: 'Arial, sans-serif', 
                    fontWeight: '500' 
                }
            }
        },
        tooltip: {
            animation: 2,
            shared:true

        },
        series: [
            {
                name: "Won toss",
                data: listOfTeamsWithTossWonAndMatchesWon.map((team) => team[1].won_toss)
            }, {
                name: "Won toss",
                data: listOfTeamsWithTossWonAndMatchesWon.map((team) => team[1].won_match)
            }
        ]
    })


    Highcharts.chart('plot-five', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top ten bowlers economy',
            align: 'left',
            x: 70,
            style: {
                color: '#100445',
                fontSize: '27px',
                fontFamily: 'Arial, sans-serif',
                fontWeight: "600"
            }
        },
        xAxis: {
            categories: Object.keys(topTenEconomicalBowlersIn2015),
            crosshair: true,
            labels:{
                rotation:0,
                formatter: function() {
                    return this.value.replace(' ', '<br/>');
                  }
            },
            labels: {
                rotation:0,
                formatter: function() {
                    return this.value.replace(' ', '<br/>');
                  },
                style: {
                    color: 'black',
                    fontSize: '13px',
                    fontFamily: 'Arial, sans-serif',
                    fontWeight: '500' 
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Economy of bowler',
                style: {
                    color: 'black', 
                    fontSize: '22px', 
                    fontFamily: 'Arial, sans-serif', 
                    fontWeight: '500' 
                }
            }
        },
        tooltip: {
            animation: 2,
            shared:true
        },
        series: [
            {
                name: "Economy",
                data: listOfTopTenBowlers.map((team) => team[1].economy),
                color:'#d1584f'
            }, 
        ]
    })

    Highcharts.chart('plot-six', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Most man of the matches by a player in each season',
            align: 'left',
            x: 70,
            style: {
                color: '#100445',
                fontSize: '27px',
                fontFamily: 'Arial, sans-serif',
                fontWeight: "600"
            }
        },
        xAxis: {
            categories: Object.keys(playerWhoWonMostMOM),
            crosshair: true,
            labels: {
                style: {
                    color: 'black',
                    fontSize: '13px',
                    fontFamily: 'Arial, sans-serif',
                    fontWeight: '500' 
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of MOM',
                style: {
                    color: 'black', 
                    fontSize: '22px', 
                    fontFamily: 'Arial, sans-serif', 
                    fontWeight: '500' 
                }
            }
        },
        tooltip: {
            animation: 2,
            shared: true
        },
        series: [
            {
                name: "Season",
                data: listOfPlayerWithMostMOM.map((player) => player[1]),
            },
        ]
    })

    let batters = strikeOfBatsmanInEachSeason.reduce((acc, season) => {
        acc = new Set([...acc, ...Object.entries(season[1]).map((batter) => batter[0])])
        return acc
    }, new Set())
    batters = Array.from(batters);
    Highcharts.chart('plot-seven', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Strikerate of batsman in each season',
            align: 'left',
            x: 70,
            style: {
                color: '#100445',
                fontSize: '27px',
                fontFamily: 'Arial, sans-serif',
                fontWeight: "600"
            }
        },

        xAxis: {
            categories: strikeOfBatsmanInEachSeason.map((season) => season[0]),
            crosshair: true,
            labels: {
                style: {
                    color: 'black',
                    fontSize: '13px',
                    fontFamily: 'Arial, sans-serif',
                    fontWeight: '500' 
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Strike-rate',
                style: {
                    color: 'black', 
                    fontSize: '22px', 
                    fontFamily: 'Arial, sans-serif', 
                    fontWeight: '500' 
                }
            }
        },
        tooltip: {
            animation: 2,
            shared: true,
        },
    series: batters.map((batter) => {
        return {
            name: batter,
            pointPadding: 0.05,
            groupPadding: 0.1,
            data: strikeOfBatsmanInEachSeason.map((season) => {
                return season[1][batter] || 0;
            })
        }
    })
    })
    let seriesData=(numberOfTimesPlayerDismissalAnotherPlayer.map((bowler)=>{
        return [bowler[0]+" - "+bowler[1][0],bowler[1][1]]
    })).slice(0,1000)
    Highcharts.chart('plot-eight', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Number of times a batsman dismissed by a batsman',
            align: 'left',
            x: 70,
            style: {
                color: '#100445',
                fontSize: '27px',
                fontFamily: 'Arial, sans-serif',
                fontWeight: "600"
            }
        },
        xAxis: {
            categories: numberOfTimesPlayerDismissalAnotherPlayer.map((bowler)=>{
                return bowler[0]
            }).slice(0,1000),
            crosshair: true,
            labels: {
                style: {
                    color: 'black',
                    fontSize: '13px',
                    fontFamily: 'Arial, sans-serif',
                    fontWeight: '500' 
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of dismissals',
                style: {
                    color: 'black', 
                    fontSize: '22px', 
                    fontFamily: 'Arial, sans-serif', 
                    fontWeight: '500' 
                }
            }
        },
        tooltip: {
            animation: 2
        },
        series: [{
            name: 'number of times dismissed',
            data:seriesData,
        }]
    })

    Highcharts.chart('plot-nine', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Best Economy in Super over',
            align: 'left',
            x: 70,
            style: {
                color: '#100445',
                fontSize: '27px',
                fontFamily: 'Arial, sans-serif',
                fontWeight: "600"
            }
        },
        xAxis: {
            categories: Object.keys(bestEconomyInSuperOver),
            crosshair: true,
            labels: {
                style: {
                    color: 'black',
                    fontSize: '13px',
                    fontFamily: 'Arial, sans-serif',
                    fontWeight: '500' 
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Economy',
                style: {
                    color: 'black', 
                    fontSize: '22px', 
                    fontFamily: 'Arial, sans-serif', 
                    fontWeight: '500' 
                }
            }
        },
        tooltip: {
            animation: 2
        },
        series: [{
            name: 'Economy',
            data: Object.values(bestEconomyInSuperOver),
            color:'green'
        }]
    })
}

document.addEventListener('DOMContentLoaded', function() {
    plotData()
  });
  

